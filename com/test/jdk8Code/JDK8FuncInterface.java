package com.test.jdk8Code;

//Sample code to create own functional interface
@FunctionalInterface // Optional
interface Call{
    abstract void mobile();
}

public class JDK8FuncInterface implements Call{
    //Override call method
    public void mobile(){
        System.out.println("Hello");
    }

    public static void main(String... args){

        System.out.println("main");
        JDK8FuncInterface t = new JDK8FuncInterface();
        //traditional way of implementation
        t.mobile();

        //lambda
        //Test
       Call c = ()-> System.out.println("mobile");
       c.mobile();
    }
}
