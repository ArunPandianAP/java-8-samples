package com.test.jdk8Code;

import java.util.ArrayList;
import java.util.List;

public class JDK8LambdaWithArrrayList {

    public static void main(String... args){
        List<String> employeeName = new ArrayList<>(5);
        employeeName.add("Arun");
        employeeName.add("vijay");
        employeeName.add("Ram");
        employeeName.add("Mohan");
        employeeName.add("Sunil");

        //Traditinal way to iterate
        for (String emp:employeeName) {
            System.out.println(emp);
        }

        // Using lambda(name==> patameter, after arrow mark logic)
        employeeName.forEach(name ->System.out.println(name));

        //Multi8 line logic
        employeeName.forEach(empname ->{
            String valueAfterConcat = empname.concat(" Test");
            System.out.println(valueAfterConcat);
        });
        System.out.println("Test Commit");
    }
}
